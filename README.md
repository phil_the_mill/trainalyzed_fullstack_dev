# README #

This repository contains some tasks for applicants as fullstack developer for [TRAINALYZED](https://trainalyzed.com/).

### Requirements:

```
pip install numpy
pip install pandas
pip install bokeh==2.0.2
```


### Important!

* Code as you would in production.
* Keep in mind to produce readable, propriated commented code (no unnecessary comment like 'this is setter...').
* Since we are hiring a fullstack developer we are also focus on FE abilities.
* We provide a .fit-file already in csv that you do not have to deal with our fitparser here. We highly recommend [pandas](https://pandas.pydata.org/) for handling this data.

### Task1:

TODO: 
In this task you should solve the following "warm-up" task.

* You get an input file: ./resources/task1.txt 
* This file consists of whitespace separated signed integers
* You should order this sequence and output the ordered sequence
* Not mandatory: In addition, its a plus if you also output every second prime number of the ordered list (1 is NOT prime!)

Example:
input: 
14 102 55 3 7 22 17 98 29
output: 
ordered-list: 3 7 14 17 22 29 55 98 102 
primes: 7 29

Please make sure that the output is exactly of this form:
ordered-list: ...
primes: ...


### Task2:

TODO:
We are also interested into you database modeling strategies. Assume you have to create a database scheme for commenting trainingdays. 
Use any tool (e.g. (drawio)[https://app.diagrams.net/]) you like to create an ER-diagram (specific notation does not matter).

* Training day:
    * can have one or multiple trainings
    * have a date
    * can have comments
* Training:
    * have a duration
    * have a date 
    * have an athlete performing this training
    * have training content, e.g. intervals like 4x5min Vo2max, every 30 min. 6 sec. sprint,... 
    * can have comments
* Comments:
    * have a datetime
    * have a comment text
    * are commented by any user
* User:
    * have a nickname
    * have a password
    * have email address
    * can comment
* athlete:
    * is an user
    * is performing trainings
* coach:
    * is an user


### Task3:

TODO: 
In this task you are working in a minimalistic Angular app. The goal is to create e new component "MarkerChartComponent" that fulfills the following requirements:
* is using [Bokeh](https://docs.bokeh.org/en/latest/index.html)
* is plotting the "power" series from the trainingdata file: ./resources/test.csv
* it should be possible to set 2 markers on the x-axis [x_1, x_2] (see: figure below)
* afterwards for the marked interval [x_1, x_2] the min, max and average values should be calculated

![Example task3](resources/task3_descr.png)

In this task you have more freedom to solve this problem

```
cd task3/my-app
npm install
ng serve --open
```

### After solving the task:

When you think you are done with the tasks you can compress this folder and send it to: 
If you have any issues you can write to: philip.handl@gmail.com

#GLHF
